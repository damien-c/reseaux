package exercice1;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;


public class ReceiveUDP {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		if(args.length < 1) return;
		/*
		 * args[0] = Port
		 */
		byte[] receiveBuffer  = new byte[1000];
		DatagramPacket packet = new DatagramPacket(receiveBuffer, receiveBuffer.length) ;
		
		try {
			DatagramSocket socket = new DatagramSocket(Integer.parseInt(args[0]));
			while(true) {
				try {
					socket.receive(packet);
				} catch (IOException e) {
					e.printStackTrace();
				}
				String msg = new String(packet.getData());
				System.out.println(msg);	
			}
			//socket.close();
			//socket.disconnect();
			
		} catch (SocketException e) {
			e.printStackTrace();
		}


	}

}
