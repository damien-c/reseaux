package exercice1;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class SendUDP {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		if(args.length < 3) return;
		/*
		 * args[0] = Adresse IP
		 * args[1] = Port
		 * args[2] = Message
		 */
		DatagramPacket packet = null;
		try {
			packet = new DatagramPacket(args[2].getBytes(),
					args[2].getBytes().length, InetAddress.getByName(args[0]),
					Integer.parseInt(args[1]));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		try {
			DatagramSocket socket = new DatagramSocket();
			try {
				socket.send(packet);
			} catch (IOException e) {
			}
			socket.close();
			socket.disconnect();
		} catch (SocketException e) {
		}
	}

}
