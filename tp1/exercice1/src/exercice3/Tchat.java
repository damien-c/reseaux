package exercice3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Tchat {

	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Thread t1 = new Thread(server());
		Thread t2 = new Thread(client());
		t1.start();
		t2.start();
	}

	private static Runnable server() {
		return new Runnable() {
			public void run() {
				byte[] receiveBuffer  = new byte[1000];
				DatagramPacket p = new DatagramPacket(receiveBuffer, receiveBuffer.length) ;
				InetAddress group = null;
				try {
					group = InetAddress.getByName("224.0.0.1");
				} catch (UnknownHostException e) {
				}
				
				try {
					MulticastSocket s = new MulticastSocket(7654);
					s.joinGroup(group);
					while(true) {
						try {
							s.receive(p);
						} catch (IOException e) {
						}
						System.out.println(p.getAddress().getHostName()+" : "+new String(p.getData()));
						receiveBuffer = new byte[1000];
						p.setData(receiveBuffer);
					}
					//socket.close();
					//socket.disconnect();

				} catch (IOException e) {
				}
			}
		};
	}

	private static Runnable client() {
		return new Runnable() {
			public void run() {
				InetAddress group = null;
				try {
					group = InetAddress.getByName("224.0.0.1");
				} catch (UnknownHostException e) {
				}

				// Création d'un Packet
				DatagramPacket p = null;

				// Création d'une Socket Multicast
				MulticastSocket s = null;
				
				Scanner sc = new Scanner(System.in);
				
				while(true) {
					String message = sc.nextLine();
					try {
						p = new DatagramPacket(message.getBytes(),
								message.getBytes().length, group, 7654);
					} catch (NumberFormatException e) {
					}
					try {
						s = new MulticastSocket();
						s.joinGroup(group);
						try {
							s.send(p);
						} catch (IOException e) {
						}
					} catch (IOException e) {
					}
				}
			}
		};
	}

}
