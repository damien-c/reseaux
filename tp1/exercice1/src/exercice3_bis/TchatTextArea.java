package exercice3_bis;

import java.net.DatagramPacket;

import javax.swing.JTextArea;

public class TchatTextArea extends JTextArea {
	
	private static final long serialVersionUID = 1L;

	private static TchatTextArea instance = new TchatTextArea();
	
	public static TchatTextArea getInstance() {
		return instance;
	}
	
	public void addMessage(DatagramPacket p) {
		String msg = new String(p.getData()).substring(0, p.getLength()-1);
		//System.out.println(msg.length()+"\ngetLength : "+p.getLength());
		this.setText(this.getText()+"\n"+p.getAddress().getHostName()+" : "+msg);
	}
}
