package exercice3_bis;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class TchatIHM {

	private TchatTextArea textArea;
	private DatagramPacket p;
	private InetAddress group;
	private MulticastSocket s;
	private JTextField addrTextField;
	private JTextField portTextField;
	private JTextField messageTextField;
	private int port;
	
	public TchatIHM() {
		this.createFrame();
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new TchatIHM();
	}
	
	private void initTchat(String addrip, String port) {
		
		this.port = Integer.parseInt(port);
		byte[] receiveBuffer  = new byte[1000];
		this.p = new DatagramPacket(receiveBuffer, receiveBuffer.length) ;
		try {
			this.group = InetAddress.getByName(addrip);
		} catch (UnknownHostException e) {
		}
		
		try {
			s = new MulticastSocket(this.port);
			s.joinGroup(group);
		} catch (IOException e) {
		}
		
		PacketChecker pc = new PacketChecker(s, p);

		pc.addPacketListener(new PacketObserver());
		
		pc.start();
		
	}

	private void createFrame() {
		JFrame frame = new JFrame();
		JPanel principal = new JPanel();
		JPanel connexion_panel = new JPanel();
		JPanel message_panel = new JPanel();
		textArea = TchatTextArea.getInstance();
		JScrollPane p = new JScrollPane(textArea);
		addrTextField = new JTextField("224.0.0.1");
		portTextField = new JTextField("7654");
		messageTextField = new JTextField("Entrez votre message");
		JButton b_connexion = new JButton("Connexion !");
		JButton b_message = new JButton("Go !");
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenuItem exitMenuItem = new JMenuItem("Exit");

		// Ajouter un menu avec le bouton Exit
		fileMenu.add(exitMenuItem);
		menuBar.add(fileMenu);
		frame.setJMenuBar(menuBar);

		// Définir les Layout pour chaque Panel
		principal.setLayout(new BorderLayout());
		connexion_panel.setLayout(new GridLayout(1, 3));
		message_panel.setLayout(new GridLayout(1, 2));

		// Définir le panel principal
		frame.setContentPane(principal);
		
		// Ajouter les composants au panel de connexion
		connexion_panel.add(addrTextField);
		connexion_panel.add(portTextField);
		connexion_panel.add(b_connexion);
		message_panel.add(messageTextField);
		message_panel.add(b_message);

		// Ajouter les composants au panel principal
		principal.add(p, BorderLayout.CENTER);
		principal.add(connexion_panel, BorderLayout.NORTH);
		principal.add(message_panel, BorderLayout.SOUTH);

		// Mettre la fenêtre en plein écran
		frame.setMinimumSize(new Dimension(700, 500));
		// frame.setExtendedState(JFrame.MAXIMIZED_BOTH);

		// Ajouter les listener
		frame.addWindowListener(new FermeWindowEvent());
		exitMenuItem.addActionListener(new ExitMenuListener());
		b_connexion.addActionListener(new ConnexionListener());
		b_message.addActionListener(new MessageListener());

		// Afficher la frame
		frame.setVisible(true);		
	}
	
	private class ConnexionListener implements ActionListener {

		/**
		 * Initialise le Tchat
		 */
		public void actionPerformed(ActionEvent arg0) {
			/*ConnexionListener thread = new ConnexionListener();
			thread.start();*/
			  new Thread(new Runnable() {
			      public void run() {
			        	String ip = TchatIHM.this.addrTextField.getText();
			  			String port = TchatIHM.this.portTextField.getText();
			  			TchatIHM.this.initTchat(ip, port);
			      }
			
			  }).start();
		}
		
		/*public void run() {
			String ip = TchatIHM.this.addrTextField.getText();
			String port = TchatIHM.this.portTextField.getText();
			TchatIHM.this.initTchat(ip, port);	
		}*/
	}
	
	private class MessageListener implements ActionListener {

		/**
		 * Envoie un message
		 */
		public void actionPerformed(ActionEvent arg0) {
			  new Thread(new Runnable() {
			      public void run() {
						String msg = TchatIHM.this.messageTextField.getText();
						TchatTextArea t = TchatTextArea.getInstance();
						t.addMessage(new DatagramPacket(msg.getBytes(),
								msg.getBytes().length, group, port));
			      }
			
			  }).start();
		}
		
		/*public void run() {
			String msg = TchatIHM.this.messageTextField.getText();
			TchatTextArea t = TchatTextArea.getInstance();
			t.addMessage(new DatagramPacket(msg.getBytes(),
					msg.getBytes().length, group, port));
		}*/
	}
	
	private class ExitMenuListener implements ActionListener {

		/**
		 * Ferme l'application
		 */
		public void actionPerformed(ActionEvent arg0) {
			System.exit(0);
		}
	}

	private class FermeWindowEvent extends WindowAdapter {

		/**
		 * Ferme l'application lors de l'appui sur la croix
		 */
		public void windowClosing(WindowEvent e) {
			System.exit(0);
		}
	}

}
