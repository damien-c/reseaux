package exercice3_bis;

import java.net.DatagramPacket;
import java.util.EventObject;

public class PacketEvent extends EventObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DatagramPacket p;

	public PacketEvent(Object source, DatagramPacket p) {
		super(source);
		this.p = p;
	}

	public DatagramPacket getPacket() {
		return p;
	}

}
