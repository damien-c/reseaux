package exercice3_bis;

import java.util.EventListener;

public interface PacketListener extends EventListener {
	
	public void packetAdded(PacketEvent e);
}
