package exercice3_bis;

public class PacketObserver implements PacketListener {

	@Override
	public void packetAdded(PacketEvent e) {
		TchatTextArea t = TchatTextArea.getInstance();
		t.addMessage(e.getPacket());
	}

}
