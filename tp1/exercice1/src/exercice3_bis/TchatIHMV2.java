package exercice3_bis;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class TchatIHMV2 {	
	
	private TchatTextArea textArea;
	private JTextField addrTextField;
	private JTextField portTextField;
	private JTextField messageTextField;
	
	public TchatIHMV2() {
		this.createFrame();
		Thread t1 = new Thread(server());
		//Thread t2 = new Thread(client());
		t1.start();
		//t2.start();
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new TchatIHMV2();
	}

	private static Runnable server() {
		return new Runnable() {
			public void run() {
				byte[] receiveBuffer  = new byte[1000];
				DatagramPacket p = new DatagramPacket(receiveBuffer, receiveBuffer.length) ;
				InetAddress group = null;
				try {
					group = InetAddress.getByName("224.0.0.1");
				} catch (UnknownHostException e) {
				}
				
				try {
					MulticastSocket s = new MulticastSocket(7654);
					s.joinGroup(group);
					while(true) {
						try {
							s.receive(p);
						} catch (IOException e) {
						}
						//System.out.println(p.getAddress().getHostName()+" : "+new String(p.getData()));
						TchatTextArea.getInstance().addMessage(p);
						receiveBuffer = new byte[1000];
						p.setData(receiveBuffer);
					}
					//socket.close();
					//socket.disconnect();

				} catch (IOException e) {
				}
			}
		};
	}

	/*private static Runnable client() {
		return new Runnable() {
			public void run() {
				InetAddress group = null;
				try {
					group = InetAddress.getByName("224.0.0.1");
				} catch (UnknownHostException e) {
				}

				// Création d'un Packet
				DatagramPacket p = null;

				// Création d'une Socket Multicast
				MulticastSocket s = null;
				
				Scanner sc = new Scanner(System.in);
				
				while(true) {
					String message = sc.nextLine();
					try {
						p = new DatagramPacket(message.getBytes(),
								message.getBytes().length, group, 7654);
					} catch (NumberFormatException e) {
					}
					try {
						s = new MulticastSocket();
						s.joinGroup(group);
						try {
							s.send(p);
						} catch (IOException e) {
						}
					} catch (IOException e) {
					}
				}
			}
		};
	}*/
	
	private void createFrame() {
		JFrame frame = new JFrame();
		JPanel principal = new JPanel();
		JPanel connexion_panel = new JPanel();
		JPanel message_panel = new JPanel();
		textArea = TchatTextArea.getInstance();
		JScrollPane p = new JScrollPane(textArea);
		addrTextField = new JTextField("224.0.0.1");
		portTextField = new JTextField("7654");
		messageTextField = new JTextField("Entrez votre message");
		JButton b_connexion = new JButton("Connexion !");
		JButton b_message = new JButton("Go !");
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenuItem exitMenuItem = new JMenuItem("Exit");

		// Ajouter un menu avec le bouton Exit
		fileMenu.add(exitMenuItem);
		menuBar.add(fileMenu);
		frame.setJMenuBar(menuBar);

		// Définir les Layout pour chaque Panel
		principal.setLayout(new BorderLayout());
		connexion_panel.setLayout(new GridLayout(1, 3));
		message_panel.setLayout(new GridLayout(1, 2));

		// Définir le panel principal
		frame.setContentPane(principal);
		
		// Ajouter les composants au panel de connexion
		connexion_panel.add(addrTextField);
		connexion_panel.add(portTextField);
		connexion_panel.add(b_connexion);
		message_panel.add(messageTextField);
		message_panel.add(b_message);

		// Ajouter les composants au panel principal
		principal.add(p, BorderLayout.CENTER);
		principal.add(connexion_panel, BorderLayout.NORTH);
		principal.add(message_panel, BorderLayout.SOUTH);

		// Mettre la fenêtre en plein écran
		frame.setMinimumSize(new Dimension(700, 500));
		// frame.setExtendedState(JFrame.MAXIMIZED_BOTH);

		// Ajouter les listener
		frame.addWindowListener(new FermeWindowEvent());
		exitMenuItem.addActionListener(new ExitMenuListener());
		b_connexion.addActionListener(new ConnexionListener());
		b_message.addActionListener(new MessageListener());

		// Afficher la frame
		frame.setVisible(true);		
	}
	
	private class ConnexionListener implements ActionListener {

		/**
		 * Initialise le Tchat
		 */
		public void actionPerformed(ActionEvent arg0) {
			String ip = addrTextField.getText();
  			String port = portTextField.getText();
  			
		}
	}
	
	private class MessageListener implements ActionListener {

		/**
		 * Envoie un message
		 */
		public void actionPerformed(ActionEvent arg0) {
			String msg = messageTextField.getText();
			TchatTextArea t = TchatTextArea.getInstance();
			InetAddress group = null;
			try {
				group = InetAddress.getByName("224.0.0.1");
			} catch (UnknownHostException e) {
			}
			t.addMessage(new DatagramPacket(msg.getBytes(),
					msg.getBytes().length, group, 7654));
		}

	}
	
	private class ExitMenuListener implements ActionListener {

		/**
		 * Ferme l'application
		 */
		public void actionPerformed(ActionEvent arg0) {
			System.exit(0);
		}
	}

	private class FermeWindowEvent extends WindowAdapter {

		/**
		 * Ferme l'application lors de l'appui sur la croix
		 */
		public void windowClosing(WindowEvent e) {
			System.exit(0);
		}
	}

}

