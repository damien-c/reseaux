package exercice3_bis;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;
import java.util.ArrayList;

import javax.swing.Timer;

public class PacketChecker {

	private static final int DEFAULT_PERIOD = 1000;
	private MulticastSocket s;
	private DatagramPacket p;
	private ArrayList<PacketListener> listeners;
	private Timer timer;

	public PacketChecker(MulticastSocket s, DatagramPacket p) {
		this.s = s;
		this.p = p;
		this.listeners = new ArrayList<PacketListener>();
		this.timer = new Timer(DEFAULT_PERIOD, new TimeActionListener());
	}

	/**
	 * Permet de démarrer le Timer
	 */
	public void start() {
		this.timer.start();
	}

	/**
	 * Permet d'abonné un client
	 * 
	 * @param listener
	 *            Un client
	 */
	public synchronized void addPacketListener(PacketListener listener) {
		if (!this.listeners.contains(listener))
			this.listeners.add(listener);
	}

	/**
	 * Permet de désabonné un client
	 * 
	 * @param listener
	 *            Un client
	 */
	public synchronized void removePacketListener(PacketListener listener) {
		this.listeners.remove(listener);
	}

	/**
	 * Méthode de propagation d'un êvènement.
	 * 
	 * @param filename
	 *            Le nom du fichier à ajouter
	 */
	private void firePacketAdded(DatagramPacket p) {
		// Copie de la liste
		@SuppressWarnings("unchecked")
		ArrayList<PacketListener> cloneListener = (ArrayList<PacketListener>) this.listeners
				.clone();

		// Envoyer l'évênement
		if (cloneListener.size() > 0) {
			PacketEvent e = new PacketEvent(this, p);
			for (PacketListener l : cloneListener) {
				l.packetAdded(e);
			}
		}
	}

	private class TimeActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			new Thread(new Runnable() {
			      public void run() {
					try {
						s.receive(p);
					} catch (IOException e1) {
		
					}
			      }
			  }).start();
			/*System.out.println("HostName : " + p.getAddress().getHostName()
					+ "\nHostAdresse : " + p.getSocketAddress().toString());*/

			firePacketAdded(p);

		}
	}

}
