package exercice2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

public class ReceiveMulticastUDP {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		if(args.length < 2) return;
		/*
		 * args[0] = Adresse IP
		 * args[1] = Port
		 */
		byte[] receiveBuffer  = new byte[1000];
		DatagramPacket p = new DatagramPacket(receiveBuffer, receiveBuffer.length) ;
		InetAddress group = null;
		try {
			group = InetAddress.getByName(args[0]);
		} catch (UnknownHostException e) {
		}
		
		try {
			MulticastSocket s = new MulticastSocket(Integer.parseInt(args[1]));
			s.joinGroup(group);
			while(true) {
				try {
					s.receive(p);
				} catch (IOException e) {
				}
				String msg = new String(p.getData());
				System.out.println(msg);	
			}
			//socket.close();
			//socket.disconnect();

		} catch (IOException e) {
		}
	}
}
