package exercice2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

public class SendMulticastUDP {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		if (args.length < 3)
			return;
		/*
		 * args[0] = Adresse IP 
		 * args[1] = Port 
		 * args[2] = Message
		 */
		InetAddress group = null;
		try {
			group = InetAddress.getByName(args[0]);
		} catch (UnknownHostException e) {
		}

		// Création d'un Packet
		DatagramPacket p = null;
		try {
			p = new DatagramPacket(args[2].getBytes(),
					args[2].getBytes().length, group, Integer.parseInt(args[1]));
		} catch (NumberFormatException e) {
		}

		// Création d'une Socket Multicast
		try {
			MulticastSocket s = new MulticastSocket();
			s.joinGroup(group);
			try {
				s.send(p);
			} catch (IOException e) {
			}
			s.close();
			s.disconnect();
		} catch (IOException e) {
		}
	}

}
