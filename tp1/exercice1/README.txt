Exercice 2 :

Q1. Emission : Les étapes sont : 
	- Création d'un groupe de type 'InetAddress'
	- Création d'un paquet de type 'DatagramPacket' en lui associant le groupe (l'adresse IP multicast) et le port sur lequel envoyer 			le paquet.
	- Création d'une socket de type 'MulticastSocket' et joindre la socket au groupe de la première étape
	- Envoyer le paquet à partir de la socket
Réception : Les étapes sont à peu près identiques à la différnece près qu'on attend de recevoir un paquet au lieu d'en envoyer un.

Q2. Les exceptions sont :
	- 'UnknownHostException' si l'adresse IP n'existe pas
	- 'NumberFormatException', Cette exception est levé lorsque l'on essaie de donner un port qui n'est pas un nombre. Dans ce cas, 	la conversion du port sous forme de chaine de caractères en int lancera une exception.
	- 'IOException' si la socket n'arrive pas à pointer vers l'adresse IP et/ou si l'envoie d'un paquet se passe mal.

Exercice 3 :

Q1. On peut y parvenir en insérant chaque programme dans un thread. Dans notre programme de chat, on insère le programme d'émission de paquets dans un thread et le programme de réception de paquets dans un autre thread. Ensuite, il nous suffit de démarrer les deux threads pour que les deux programmes fonctionne en même temps.

Q2. Ma solution a été de mettre le HostName de la machine devant son message en écrivant ceci : p.getAddress().getHostName()
